package lcc.informatica.uma.spark;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class SparkDemoCountFindCharactersInFiles {
	static Logger log = Logger.getLogger(SparkDemoCountFindCharactersInFiles.class.getName());

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
    	
		//Oculta errores no relacionados con la ejecucion del programa
    	Logger.getLogger("lcc").setLevel(Level.OFF);
		if (args.length < 1) {
			log.fatal("Syntax Error: there must be one argument (a file name)");
		}
    	
    	//Step 1: create a SparKConf object
    	SparkConf sparkConf = new SparkConf();
    	
    	//Step 2: create a JAva Spark Context
    	JavaSparkContext context = new JavaSparkContext(sparkConf);
    	
    	//Esto funciona aun teniendo un fichero de tamaño mayor que la memoria principal, pues está implementado como computación perezosa.
    	//Esto significa que solo lee del fichero cuando lee la sentencia de reducir, pues es el momento en el que necesita los datos.
    	JavaRDD<String> lines = context.textFile(args[0]);
    	
    	JavaRDD<String> columnaTweets = lines.flatMap(s -> Arrays.asList(s.split("[\t]")[2]).iterator());
    	
    	JavaRDD<String> words = columnaTweets.flatMap(s -> Arrays.asList(s.split(" ")).iterator());
    	
    	ArrayList<String> extensions = new ArrayList<String>();
    	extensions.add("RT");
    	
        String regexMostCommonWords = "http(.*)|rt|i| ||-|is|set|has|the|be|to|of|and|a|in|that|have|I|it|for|not|on|with|he|as|you|do|at|this|but|his|by|from|they|we|say|her|she|or|an|will|my|one|all|would|there|their|what|so|up|out|if|about|who|get|which|go|me|when|make|can|like|time|no|just|him|know|take|people|into|year|your|good|some|could|them|see|other|than|then|now|look|only|come|its|over|think|also|back|after|use|two|how|our|work|first|well|way|even|new|want|because|any|these|give|day|most|us|(.*):";
    	
    	JavaRDD<String> filteredWords = words.filter(p -> !p.toLowerCase().matches(regexMostCommonWords) );
    	
    	JavaPairRDD<String, Integer> ones = filteredWords.mapToPair(s -> new Tuple2<>(s, 1));
    	
    	JavaPairRDD<String, Integer> counts = ones.reduceByKey((integer, integer2) -> integer + integer2);    			
    	
		JavaPairRDD<Integer, String> swappedPairs = counts.mapToPair( x -> x.swap());
		 
		
    	java.util.List<Tuple2<Integer,String>> output = (List<Tuple2<Integer, String>>) swappedPairs.sortByKey().collect();
    	    	
		PrintWriter writer = new PrintWriter("trendingTopicsResults.txt", "UTF-8");

    	/*for (Tuple2<?,?> tuple : output) {
    		//System.out.println(tuple._2() + ": " + tuple._1());
    		writer.println(tuple._2() + ": " + tuple._1());
    	}*/
    	for (int i = output.size()-1; i>output.size()-21; i--){
    		writer.println(output.get(i)._2() + ": " + output.get(i)._1());
    	}
		writer.close();    	
        context.stop();
        
        //Ejecucion despues de compilar con mvn package:
        //spark-submit --class "lcc.informatica.uma.spark.AddNumbersFromFiles" .\target\spark-0.0.1-SNAPSHOT.jar .\data\numbers.txt
        //Si se quiere indicar el numero de cores, en el ejemplo 8 cores.
        //spark-submit --class --master local[8] "lcc.informatica.uma.spark.AddNumbersFromFiles" .\target\spark-0.0.1-SNAPSHOT.jar .\data\numbers.txt
//        spark-submit --class "lcc.informatica.uma.spark.SparkDemoCountFindCharactersInFiles" .\target\spark-0.0.1-SNAPSHOT.jar .\data\tweets.txt
  
	}

}
